Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0037-placer-gs).

|document|download options|
|:-----|-----:|
|operating manual           ||
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0037-placer-gs/-/raw/main/02_assembly_drawing/s2-0037-A_ZNB_placer_gh-s.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0037-placer-gs/-/raw/main/03_circuit_diagram/S2-0037_Greiferkopf_GH-S-B.pdf)|
|maintenance instructions   ||
|spare parts                ||

<!-- 2021 (C) Häcker Automation GmbH -->
